import axios from "axios";
import React, { useEffect, useState } from "react";
import "./App.css";

const baseURL = "https://randomuser.me/api/?results=100";

// var data = [];

function App() {
  const [expandedCountry, setExpandedCountry] = useState(null);
  const [data, setData] = useState([]);
  const [genderFilter, setGenderFilter] = useState("All"); // Default filter

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await axios.get(baseURL);
    setData(res.data.results);
  };

  const countryUserCounts = {};
  data.forEach((profile) => {
    const country = profile.location.country;
    if (countryUserCounts[country]) {
      countryUserCounts[country]++;
    } else {
      countryUserCounts[country] = 1;
    }
  });

  const countryUserCountsArray = Object.entries(countryUserCounts).map(
    ([country, count]) => ({
      country,
      count,
    })
  );
  // console.log("countryUserCountsArray", countryUserCountsArray);

  countryUserCountsArray.sort((a, b) => b.count - a.count);

  function CountryUsers({ country, genderFilter }) {
    let countryUsers = data.filter(
      (profile) => profile.location.country === country
    );
    // console.log("CountryUsers", countryUsers);
    if (genderFilter !== "All") {
      countryUsers = countryUsers.filter(
        (user) => user.gender === genderFilter
      );
    }
    countryUsers.sort(
      (a, b) => new Date(b.registered.date) - new Date(a.registered.date)
    );

    return (
      <div class="card card-data ">
        {countryUsers.map((user, index) => (
          <div class="card-body" key={index}>
            <div>
              <strong>Name:</strong> {user.name.first} {user.name.last}
            </div>
            <div>
              <strong>Gender:</strong> {user.gender}
            </div>
            <div>
              <strong>City:</strong> {user.location.city}
            </div>
            <div>
              <strong>State:</strong> {user.location.state}
            </div>
            <div>
              <strong>Date Registered:</strong>{" "}
              {new Date(user.registered.date).toLocaleDateString()}
            </div>
            {/* <div>
              <hr></hr>
            </div> */}
          </div>
        ))}
      </div>
    );
  }

  return (
    <div className="App">
      <h1>User Counts by Country and user details</h1>
      <label>
        Filter by Gender:
        <select
          className="selectField"
          value={genderFilter}
          onChange={(e) => setGenderFilter(e.target.value)}
        >
          <option value="All">All</option>
          <option value="male">Male</option>
          <option value="female">Female</option>
        </select>
      </label>

      <ul class="list-group">
        {countryUserCountsArray.map((countryData, index) => (
          <li
            class="list-group-item"
            style={{ cursor: "pointer" }}
            key={index}
            onClick={() => setExpandedCountry(countryData.country)}
          >
            <h2 style={{ float: "left" }}>
              {countryData.country}: {countryData.count} users
            </h2>
            {expandedCountry === countryData.country && (
              <CountryUsers
                country={countryData.country}
                genderFilter={genderFilter}
              />
            )}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default App;
